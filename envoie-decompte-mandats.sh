#!/bin/bash

set -xe

adresse_mail="contact@laquadrature.net";
path_mandat_csv="/var/www/mandat.technopolice.fr/plaintes/mandats.csv";

mail -s "Décompte des mandats technopolice pour le $(date --iso)" -r mandat@technopolice.fr $adresse_mail <<< "Bonjour, nous avons $(wc -l $path_mandat_csv | cut -d " " -f 1 ) mandats aujourd'hui. Passez une belle journée. ( Ce message à été envoyé automatiquement )."   
