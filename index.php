<!DOCTYPE html>
<html>

<head>
	<title>Formulaire</title>
	<meta charset="UTF-8">
	<link rel="stylesheet" id="style-css"  href="style.css" type="text/css" media="all" />
	<link rel="shortcut icon" href="favicon.png" sizes="192x192" />
</head>

<body>

<section class="intro">

<div class="content">

<?php


// Adresses générales

	$organisation = "Organisation"

	$email_contact = "contact@organisation.org"

	// Dossier contenant les fichiers
	$dir = "/var/www/plaintes-collectives/plaintes/";

	// Fichier des mandats en attente de validation
	$waiting_file = $dir . "attente.csv";

	// Fichier des mandats validés
	$mandate_file = $dir . "mandats.csv";

	// URL du site
	$url = "";

	// URL de confirmation
	$confirm_url = $url . "/index.php?k=";

// Traitement du formulaire, appelé par l'envoie du formulaire par les clientes
if (isset($_POST["name"])) {
	// Récupère les informations du formulaire (nom et adresse) en les limitant à 100 caractères
	$name = substr(htmlspecialchars($_POST["name"]), 0, 100);
	$email = substr(htmlspecialchars($_POST["email"]), 0, 100);

	// Génère une clef aléatoire (utilisé pour le lien de confirmation)
	$key = bin2hex(openssl_random_pseudo_bytes (8));

	// Définit le contenu du nouveau mandat
	$new_mandate =  [
		$name,
		$email,
		date("d/m/Y"),
		$key
	];

	// Ajoute le nouveau mandat à la liste des mandats en attente de validation
	file_put_contents ( $waiting_file, join($new_mandate, ";") . "\n",  FILE_APPEND );

	// Mail de confirmation
	$message = "Bonjour " . $name . ".\n\n";

	$message .= "Pour mandater ". $organisation ." en votre nom contre le système de surveillance de l'espace public mis en place par l'État français, tel que décrit sur " . $url . ",\n\n";

	$message .= "Cliquez sur : \n" . $confirm_url . $key . "\n\n";

	$message .= "Le nom que vous avez indiqué dans le formulaire sera transmis à la CNIL en même temps que la plainte collective. ". $organisation ." conservera ce nom et votre adresse email tout le long de la procédure, afin d'en assurer le bon déroulé et de vous transmettre les informations et décisions que la CNIL chercherait à vous communiquer.\n\n";

	$message .= "Pour révoquer votre mandat et supprimer vos informations, écrivez-nous depuis cette même adresse à ". $email_contact ."\n\n";

	$message .= "Si vous n'êtes pas à l'origine de ce message, vous pouvez l'ignorer ; la présente demande ne sera alors pas prise en compte.";

	// Limite les lignes à 70 caractères
	$message = wordwrap($message, 70, "\n");

	// Envoie le mail
	mail(htmlspecialchars($_POST["email"]), 'Plainte collective contre la Technopolice', $message, "From: La Quadrature du Net <contact@laquadrature.net>\r\nContent-type: text/plain; charset=UTF-8");

	// Indique aux clientes qu'un email a été envoyé
	echo "<h1>Confirmez par email</h1>Un email de confirmation vient de vous être envoyé : cliquez sur le lien indiqué dans l'email afin de valider votre mandat. Merci !<br><br><a href='". $url . "'>Retourner</a> vers le site.";

}

// Traitement de la confirmation, appelé par un clic sur le lien de confirmation
if (isset($_GET["k"]))
{
	// Récupère le contenu de la liste d'attente
	$waiting_list = file ($waiting_file);

	// Récupère le contenu des mandats validés
	$mandates = file ($mandate_file);

	// Récupère la clef du lien de confirmation
	$key = htmlspecialchars($_GET["k"]);

	// Recherche l'index du mandat associé à la clef
	$index = sub_in_array($waiting_list, ";" . $key ."\n");

	// Si le mandat a été retrouvé
	if ($index !== "none")
	{
		// Recupère le contenu du nouveau mandat dans un tableau
		$new_mandate = explode(";", $waiting_list[$index]);

		// Retire la clef au mandat (elle ne sera plus utile)
		array_pop($new_mandate);

		// Si aucun mandat n'est déjà enregistré sous la même adresse
		if(sub_in_array($mandates, $new_mandate[1]) == "none"){

			// Ajoute le nouveau mandat à la liste des mandats validés
			file_put_contents
			(
				$mandate_file,
				join($new_mandate, ";") . "\n",
				FILE_APPEND
			);
		}

		// Supprime le nouveau mandat de la liste d'attente
		$waiting_list[$index] = "";

		// Remplace la liste d'attente
		file_put_contents ($waiting_file, join($waiting_list, ""));

		// Indique que la mandat a bien été enregistré
		echo "<h1>Merci</h1>Votre mandat a bien été enregistré.<br><br><a href='" . $url . "'>Retourner</a> vers le site.";
	}

	// Si la clef n'a pas été trouvée, indique que l'enregistrement a échoué
	else {
		echo "<h1>Erreur</h1>Une erreur est survenue lors de l'enregistrement de votre demande.<br><br>Merci de remplir un nouveau formulaire.<br><br><a href='" . $url . "'>Retourner</a> vers le site.";
	}
}
// Renvoie l'index du premier élément d'un tableau (arr) qui contient une sous-chaine (sub)
function sub_in_array($arr, $sub)
{
	foreach ($arr as $index => $value)
	{
		if ( strstr($value, $sub) )
		{
			return $index;
		}
	}
	return "none";
}

?>

</div>
</section>
</body>
</html>
